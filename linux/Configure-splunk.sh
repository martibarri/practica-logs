#!/bin/bash

practica_logs_dir="$HOME/Desktop/practica-logs"
practica_splunk_dir="$practica_logs_dir/linux/files/splunk"
splunk_etc_dir="/opt/splunk/etc"
splunk_conf_dir="$splunk_etc_dir/users/splunk/search/local"
splunk_dashboard_dir="$splunk_conf_dir/data/ui/views"

verifyRoot () {
	# Make sure we are root before proceeding
	[ `id -u` != 0 ] && { echo "$0: Please run this as root (using sudo)."; exit 0; }
}

copyCustomFiles() {
    [ ! -d $splunk_conf_dir ] && { mkdir -p $splunk_conf_dir; }
    [ ! -d $splunk_dashboard_dir ] && { mkdir -p $splunk_dashboard_dir; }

    sudo cp $practica_splunk_dir/ui-prefs.conf $splunk_conf_dir/ui-prefs.conf
    sudo cp $practica_splunk_dir/ui-tour.conf $splunk_conf_dir/ui-tour.conf
    sudo cp $practica_splunk_dir/props.conf $splunk_conf_dir/props.conf
    sudo cp $practica_splunk_dir/eventtypes.conf $splunk_conf_dir/eventtypes.conf
    sudo cp $practica_splunk_dir/dashboard/* $splunk_dashboard_dir/
}

restartSplunk() {
    sudo /opt/splunk/bin/splunk restart
}

verifyRoot
copyCustomFiles
restartSplunk

exit 0
