#!/bin/bash

practica_logs_dir="$HOME/Desktop/practica-logs"

verifyRoot () {
	# Make sure we are root before proceeding
	[ `id -u` != 0 ] && { echo "$0: Please run this as root (using sudo)."; exit 0; }
}

cloneRepository() {
	git clone https://gitlab.com/martibarri/practica-logs.git $practica_logs_dir
	chown -R ubuntu:ubuntu $practica_logs_dir
}

configureRsyslog() {
    sudo cp $practica_logs_dir/linux/files/rsyslog-mod.conf /etc/rsyslog.conf
    sudo cp $practica_logs_dir/linux/files/50-default-mod.conf /etc/rsyslog.d/50-default.conf
    sudo /bin/systemctl restart rsyslog
}

installSplunk() {
    wget https://download.splunk.com/products/splunk/releases/7.2.6/linux/splunk-7.2.6-c0bf0f679ce9-linux-2.6-amd64.deb -O /tmp/splunk-7.2.6.deb
    sudo dpkg -i /tmp/splunk-7.2.6.deb
    sudo /opt/splunk/bin/splunk start
}


verifyRoot
cloneRepository
configureRsyslog
installSplunk

exit 0
