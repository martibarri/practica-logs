
$MasterFolder = "$([Environment]::GetFolderPath("Desktop"))\MASTER"
$DesktopFolder = "$([Environment]::GetFolderPath("Desktop"))"

# Descàrrega repo configuració 
if (Test-Path -Path "$DesktopFolder\practica-logs") {
    Remove-Item "$DesktopFolder\practica-logs" -Recurse -Force | Out-Null
}
mkdir "$DesktopFolder\practica-logs" -Force | Out-Null
& "$MasterFolder\Git\bin\git.exe" clone https://gitlab.com/martibarri/practica-logs.git "$DesktopFolder\practica-logs"

Write-Host "practica-logs.git repository downloaded correctly." -ForegroundColor Green
Write-Host "Press any key to close."
$tecla = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

