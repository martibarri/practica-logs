@echo off

net session >nul 2>&1
if not "%errorLevel%" == "0" (
    echo It is necessary to execute the script as Administrator!
    pause
    exit /b %errorLevel%
)

echo.
echo Import the view "C:\Users\IEUser\Desktop\practica-logs\windows\files\event_viewer_practica_marti.xml"  from
echo "Action" > "Import Custom View..." > "event_viewer_practica_marti.xml"
echo Import it in the default "Custom Views" folder.
pause

start "" eventvwr.msc 

