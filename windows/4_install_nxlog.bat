@echo off

net session >nul 2>&1
if not "%errorLevel%" == "0" (
    echo It is necessary to execute the script as Administrator!
    pause
    exit /b %errorLevel%
)

echo Installing NXLog-CE...
:: Atención a la versión descargada mediante Configure-Win81-IE11.ps1
msiexec /passive /i "C:\Users\IEUser\Desktop\MASTER\Downloads\nxlog-ce-2.10.2150.msi"

echo Backing up nxlog.conf...
copy /y "%ProgramFiles%\nxlog\conf\nxlog.conf" "%ProgramFiles%\nxlog\conf\nxlog.original.conf" >nul 2>&1
echo Setting up custom nxlog.conf...
copy /y "C:\Users\IEUser\Desktop\practica-logs\windows\files\nxlog.conf" "%ProgramFiles%\nxlog\conf\nxlog.conf" >nul 2>&1


echo.
echo It is necessary to edit the file "%ProgramFiles%\nxlog\conf\nxlog.conf" with the IP address of the remote Syslog.
pause
start "" notepad.exe "%ProgramFiles%\nxlog\conf\nxlog.conf"


echo .
echo 'nxlog' service needs to be restarted:
pause
start "" services.msc "nxlog"

