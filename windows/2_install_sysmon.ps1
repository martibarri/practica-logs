
$MasterFolder = "$([Environment]::GetFolderPath("Desktop"))\MASTER"
$DesktopFolder = "$([Environment]::GetFolderPath("Desktop"))"

# Install sysmon
$Arguments = "-accepteula", "-i", "$DesktopFolder\practica-logs\windows\files\sysmon_config.xml"
Start-Process -Verb runAs -FilePath $MasterFolder\Sysinternals\Sysmon.exe -ArgumentList $Arguments -wait

# Final del script (evitar que se cierre)
Write-Host "Sysmon installed correctly with $DesktopFolder\practica-logs\windows\files\sysmon_config.xml config file." -ForegroundColor Green
Write-Host "Press any key to close."
$tecla = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")


